"""Teatro UD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from rest_framework import routers
from teatros import views as theaters_views

# Api router
router = routers.DefaultRouter()
router.register('country', theaters_views.PaisViewSet, basename='Pais')
router.register('type/calendar', theaters_views.TipoCalendarioViewSet, basename='Tipo calendario')
router.register('type/artwork', theaters_views.TipoObraViewSet, basename='Tipo obra')
router.register('dramaturge', theaters_views.DramaturgoViewSet, basename='Dramaturgo')
router.register('theater', theaters_views.TeatroViewSet, basename='Teatro')

urlpatterns = [
    path('admin/', admin.site.urls),
    # core
    path('', include(('core.urls', 'core'), namespace="home")),
    # users
    path('users/', include(('users.urls', 'users'), namespace="users")),
    
    # Api routes
    path('api/', include(router.urls)),
    #path('api/country/', theaters_views.PaisViewSet.as_view({'get': 'list'}))

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)