from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from datetime import datetime
# Create your views here.
@login_required
def home_feed(request):
    return render(request, 'home/feed.html')