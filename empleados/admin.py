from django.contrib import admin
from .models import Empleado, GastoObra, ListaActividad, LaborPersonalObra, ListaGasto, Rol, Periodo, PersonalObra

# Register your models here.
admin.site.register(Empleado)
admin.site.register(GastoObra)
admin.site.register(ListaActividad)
admin.site.register(LaborPersonalObra)
admin.site.register(ListaGasto)
admin.site.register(Rol)
admin.site.register(Periodo)
admin.site.register(PersonalObra)