from datetime import datetime
from django.db import models
from users.models import Unidad
from teatros.models import Calendario, Obra 

# Create your models here.
class Empleado(models.Model):
    cod_empleado = models.CharField(max_length=4, null=False, blank=False, primary_key=True)
    cod_unidad = models.CharField(max_length=5, null=False, blank=False,)
    cedula = models.CharField(max_length=15, null=False, blank=False,)
    celular = models.CharField(max_length=15, null=False, blank=False,)
    nombre = models.CharField(max_length=30, null=False, blank=False,)
    apellido = models.CharField(max_length=30, null=False, blank=False,)
    correo = models.CharField(max_length=40, null=False, blank=False,)
    def __str__(self):
        """Return describe type person."""
        return '{} {}'.format(self.apellido, self.nombre)

class PersonalObra(models.Model):
    persona_obra = models.CharField(max_length=4, null=False, blank=False, primary_key=True)
    cod_empleado_obra = models.ForeignKey(Empleado, on_delete=models.CASCADE, null=False, blank=False,)
    cod_unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE, null=False, blank=False,)
    rol = models.ForeignKey('empleados.rol', on_delete=models.CASCADE, null=False, blank=False,)
    obra = models.ForeignKey(Obra, on_delete=models.CASCADE, null=False, blank=False,)
    fecha_inicio = models.DateField(default=datetime.now)
    fecha_fin = models.DateField(default=datetime.now)
    def __str__(self):
        """Return describe type person."""
        return self.persona_obra

class Rol(models.Model):
    rol = models.DecimalField(max_digits=4, decimal_places=1, null=False, blank=False, primary_key=True)
    def __str__(self):
        """Return describe type person."""
        return self.rol

class ListaActividad(models.Model):
    cod_actividad = models.CharField(max_length=4, null=False, blank=False, primary_key=True)
    periodo = models.ForeignKey("empleados.periodo", on_delete=models.CASCADE, null=False, blank=False,)
    desc_actividad = models.CharField(max_length=40, null=False, blank=False,)
    valor_hora = models.DecimalField(max_digits=4, decimal_places=2, null=False, blank=False,)
    def __str__(self):
        """Return describe type"""
        return self.desc_actividad

class Periodo(models.Model):
    periodo = models.DecimalField(max_digits=4, decimal_places=1, null=False, blank=False, primary_key=True)
    def __str__(self):
        """Return describe type"""
        return self.periodo

class ListaGasto(models.Model):
    cod_gasto = models.CharField(max_length=5, null=False, blank=False, primary_key=True)
    periodo = models.ForeignKey("empleados.periodo", on_delete=models.CASCADE, null=False, blank=False,)
    def __str__(self):
        """Return describe type"""
        return self.cod_gasto

class GastoObra(models.Model):    
    consec_gasto = models.DecimalField(max_digits=5, decimal_places=1, null=False, blank=False, primary_key=True)
    cod_gasto = models.ForeignKey(ListaGasto, on_delete=models.CASCADE, null=False, blank=False,)
    periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE, null=False, blank=False,)
    obra = models.ForeignKey(Obra, on_delete=models.CASCADE, null=False, blank=False,)
    fecha_gasto = models.DateField(default=datetime.now)
    def __str__(self):
        """Return describe type"""
        return self.consec_gasto_obra

class LaborPersonalObra(models.Model):
    consec_labor = models.CharField(max_length=4, null=False, blank=False, primary_key=True)
    persona_obra = models.ForeignKey(PersonalObra, on_delete=models.CASCADE, null=False, blank=False,)
    cod_empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, null=False, blank=False,)
    cod_unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE, null=False, blank=False,)
    cod_actividad = models.ForeignKey(ListaActividad, on_delete=models.CASCADE, null=False, blank=False,)
    periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE, null=False, blank=False,)
    consec_calendario = models.ForeignKey(Calendario, on_delete=models.CASCADE, null=False, blank=False,)
    obra = models.ForeignKey(Obra, on_delete=models.CASCADE, null=False, blank=False,)
    def __str__(self):
        """Return describe type person."""
        return self.persona_obra