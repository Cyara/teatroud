-- Carga de datos

-- 1. Pais
INSERT INTO TEATROS_PAIS
    VALUES('COL','170','Colombia');
INSERT INTO TEATROS_PAIS
    VALUES('GBR','826','Reino Unido');
INSERT INTO TEATROS_PAIS
    VALUES('ESP','724','España');
INSERT INTO TEATROS_PAIS
    VALUES('GRC','300','Grecia');
INSERT INTO TEATROS_PAIS
    VALUES('NOR','578','Noruega');
INSERT INTO TEATROS_PAIS
    VALUES('CAN','124','Canada');

-- 2. Dramaturgos
INSERT INTO TEATROS_DRAMATURGO
    VALUES('001', '826','William Shakespeare');
INSERT INTO TEATROS_DRAMATURGO
    VALUES('002', '724','Federico García Lorca');
INSERT INTO TEATROS_DRAMATURGO
    VALUES('003', '724','Pedro Calderón de la Barca');
INSERT INTO TEATROS_DRAMATURGO
    VALUES('004', '724','José Zorrilla');
INSERT INTO TEATROS_DRAMATURGO
    VALUES('005', '724','Lope de Vega');
INSERT INTO TEATROS_DRAMATURGO
    VALUES('006', '300','Sófocles');
INSERT INTO TEATROS_DRAMATURGO
    VALUES('007', '578','Henrik Ibsen');

-- 3. Tipo calendario
INSERT INTO TEATROS_TIPOCALENDARIO
    VALUES('01','Calendario A');
INSERT INTO TEATROS_TIPOCALENDARIO
    VALUES('02','Calendario B');
INSERT INTO TEATROS_TIPOCALENDARIO
    VALUES('03','Calendario C');
INSERT INTO TEATROS_TIPOCALENDARIO
    VALUES('04','Calendario D');
INSERT INTO TEATROS_TIPOCALENDARIO
    VALUES('05','Calendario E');
INSERT INTO TEATROS_TIPOCALENDARIO
    VALUES('06','Calendario F');
INSERT INTO TEATROS_TIPOCALENDARIO
    VALUES('07','Calendario G');

-- 5. Tipo Obra
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('01','Drama');
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('02','Tragedia');
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('03','Teatro');
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('04','Romántico');
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('05','Tragicomedia');
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('06','Novela');
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('07','Musical');
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('08','Ópera');
INSERT INTO TEATROS_TIPOOBRA 
    VALUES('09','Monólogo');

-- 5. Teatro
INSERT INTO TEATROS_TEATRO 
    VALUES('TCB', 'Cl. 10 ## 5-32, La Candelaria, Bogotá','Teatro Colón Bogotá', 'http://localhost:8080/media/obras/teatro/TPM.jpg');
INSERT INTO TEATROS_TEATRO 
    VALUES('JEG','Cra. 7 #22-47, Bogotá, Cundinamarca','Teatro Municipal Jorge Eliécer Gaitán', 'http://localhost:8080/media/obras/teatro/JEG.webp');
INSERT INTO TEATROS_TEATRO 
    VALUES('JMS','Cl. 170 ##67-51, Bogotá, Cundinamarca','Teatro Mayor Julio Mario Santo Domingo', 'http://localhost:8080/media/obras/teatro/JMS.webp');
INSERT INTO TEATROS_TEATRO 
    VALUES('TLC','Cra. 11 #61-80, Bogotá', 'Teatro Libre Chapinero', 'http://localhost:8080/media/obras/teatro/TLC.jpg');
INSERT INTO TEATROS_TEATRO 
    VALUES('ABC','Cl. 104 #1722, Bogotá', 'TEATRO ABC', 'http://localhost:8080/media/obras/teatro/ABC.webp');
INSERT INTO TEATROS_TEATRO 
    VALUES('PTM','Cra. 42 #50A-12, Medellín, Antioquia', 'Pequeño Teatro de Medellín', 'http://localhost:8080/media/obras/teatro/TPM.jpg');
INSERT INTO TEATROS_TEATRO 
    VALUES('JGG','Cl. 41 ## 57 - 30, Medellín, Antioquia', 'Teatro Metropolitano José Gutiérrez Gómez', 'http://localhost:8080/media/obras/teatro/JGG.jpg');
INSERT INTO TEATROS_TEATRO 
    VALUES('PTU','Cra. 40 #51-24, Medellín, Antioquia', 'Teatro Pablo Tobón Uribe', 'http://localhost:8080/media/obras/teatro/PTU.jpg');
INSERT INTO TEATROS_TEATRO 
    VALUES('TUM','Cl. 31b, Medellín, Antioquia', 'Teatro de la Universidad de Medellín', 'http://localhost:8080/media/obras/teatro/TUM.jpg');

-- 6. Obra To do -
INSERT INTO TEATROS_OBRA 
    VALUES('0001', CURRENT_TIMESTAMP , 'Romeo y Julieta', 1, '826', '001', 'http://localhost:8080/media/obras/poster/Romeo_y_Julieta.jpg' );
INSERT INTO TEATROS_OBRA 
    VALUES('0002', CURRENT_TIMESTAMP , 'Hamlet', 0, '826', '001', 'http://localhost:8080/media/obras/poster/hamlet.jpg' );
INSERT INTO TEATROS_OBRA 
    VALUES('0003', CURRENT_TIMESTAMP , 'La casa de Bernarda Alba', 0, '724', '002', 'http://localhost:8080/media/obras/poster/La-casa-de-Bernarda-Alba.jpg' );
INSERT INTO TEATROS_OBRA ("ID_OBRA", "FECHA_OBRA", "TITULO", "ESTADO", "COD_PAIS_ID", "ID_DRAMATURGO_ID","POSTER")
    VALUES('0004', CURRENT_TIMESTAMP , 'La Celestina', 0, '724', '002', 'http://localhost:8080/media/obras/poster/la-celestina.jpg' );
INSERT INTO TEATROS_OBRA 
    VALUES('0005', CURRENT_TIMESTAMP , 'La vida es sueño', 0, '724', '003', 'http://localhost:8080/media/obras/poster/la-vida-es-sueno.jpg' );
INSERT INTO TEATROS_OBRA 
    VALUES('0006', CURRENT_TIMESTAMP , 'Don Juan Tenorio', 0, '724', '004', 'http://localhost:8080/media/obras/poster/donjuantenorio.jpg' );
INSERT INTO TEATROS_OBRA 
    VALUES('0007', CURRENT_TIMESTAMP , 'Fuenteovejuna', 0, '724', '005', 'http://localhost:8080/media/obras/poster/fuenteovejuna.webp' );
INSERT INTO TEATROS_OBRA 
    VALUES('0008', CURRENT_TIMESTAMP , 'Edipo rey', 0, '300', '006', 'http://localhost:8080/media/obras/poster/Edipo_Rey-Sofocles-lg.png' );
INSERT INTO TEATROS_OBRA 
    VALUES('0009', CURRENT_TIMESTAMP , 'Casa de muñecas', 0, '578', '007', 'http://localhost:8080/media/obras/poster/casa_munecas.jpg' );

INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA
    VALUES(1, '0001', '03');
INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA 
    VALUES(2, '0002','04');
INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA 
    VALUES(3, '0003', '03');
INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA 
    VALUES(4, '0004', '06');
INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA 
    VALUES(5, '0005', '02' );
INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA 
    VALUES(6, '0006', '01');
INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA 
    VALUES(7, '0007', '03');
INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA 
    VALUES(8, '0008', '02');
INSERT INTO TEATROS_OBRA_ID_TIPO_OBRA 
    VALUES(9, '0009', '03');

-- 7. Estudiante