-- Database: teatro_ud

-- DROP DATABASE IF EXISTS teatro_ud;

CREATE DATABASE teatro_ud
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'es_CO.UTF-8'
    LC_CTYPE = 'es_CO.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- Agregar facultades
INSERT INTO universities_facultad (id_facultad, name_school, created, updated)
        VALUES  (1,'Facultad de Artes - ASAB', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Facultad de Ingeniería', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (3,'Facultad Tecnológica', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (4,'Facultad de Ciencias y Educación', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (5,'Facultad del Medio Ambiente y Recursos Naturales', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


-- Agregar Carreras
INSERT INTO universities_carrera (id_carrera, name_career, id_facultad_fk_id, created, updated)
        VALUES  (1,'Ingeniería Catastral y Geodesia', 2, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Administración Ambiental', 5, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (3,'Administración Deportiva', 4, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (4,'Archivística y Gestión de la Información Digital ', 4, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (5,'Arte Danzario', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (6,'Artes Escénicas', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (7,'Artes Musicales ', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (8,'Artes Plásticas y Visuales', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


-- Agregar Teatros
INSERT INTO teatros_teatro (id_teatro, name_teatro, created, updated)
        VALUES  (1,'Teatro Universidad Nacional de Colombia', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Teatro ECCI', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (3,'Teatro CUN', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- Agregar Generos
INSERT INTO teatros_genero (id_genero, genero, created, updated)
        VALUES  (1,'Tragedia', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Comedia', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (3,'Drama', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (4,'Musical', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (5,'Ópera', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (6,'Tragicomedia', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- Agregar Paises
INSERT INTO teatros_pais (id_pais, pais, created, updated)
        VALUES  (57,'Colombia', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (56,'Chile', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (53,'Cuba', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (54,'Argentina', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (297,'Aruba', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (591,'Bolivia', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (55,'Brasil', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (1,'Canadá', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (593,'Ecuador', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (503,'El Salvador', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Estados Unidos', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (502,'Guatemala', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- Agregar tipo uso teatro
INSERT INTO teatros_tipousoteatro (id_tipo, descripcion, created, updated)
        VALUES  (1,'Ensayo', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Función', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- Agregar tipo documento
INSERT INTO users_tipodocumento (id_tipo_documento, abreviacion, desc_tipo)
        VALUES  (1, 'RC','Registro civil'),
                (2, 'TI','Tarjeta de identidad'),
                (3, 'CC','Cédula de ciudadanía'),
                (4, 'CE','Cédula de extranjeria'),
                (5, 'PA','Pasaporte'),
                (6, 'VISA','Visa'),
                (7, 'NIP','Número de identificación personal'),
                (8, 'NIT','Número de identificación tributaria'),
                (9, 'NIT','No registra');


-- Agregar Horario Obra
INSERT INTO teatros_horarioteatro (id_horario, fecha, hora_inicio, hora_final, id_tipo_uso_fk_id, created, updated)
        VALUES  (1,'2022-02-22', '06:22:58.526624', '08:22:58.526624', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (3,'2022-02-24', '07:22:58.526624', '09:22:58.526624', 2, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

-- Agregar Obra
INSERT INTO teatros_obrateatro (id_obra, titulo_obra, escritor, anio, pais_id, genero_id, fecha_creacion, id_horario_fk_id, created, updated)
        VALUES  (1,'Vuelta al mundo en 50 días', 'Julio Bernandez','2022-02-22',593, 6, '2022-05-22',  2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'La divina comedia Argentina', 'Alonzo Di María','2022-02-22',54, 4, '2022-12-22',  1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);