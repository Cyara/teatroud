-- Database: teatro_ud

-- DROP DATABASE IF EXISTS teatro_ud;

CREATE DATABASE teatro_ud
    WITH 
    OWNER = system
    ENCODING = 'UTF8'
    LC_COLLATE = 'es_CO.UTF-8'
    LC_CTYPE = 'es_CO.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- Agregar facultades
INSERT INTO universities_facultad (id_facultad, name_school, created, updated)
        VALUES  (1,'Facultad de Artes - ASAB', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Facultad de Ingeniería', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (3,'Facultad Tecnológica', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (4,'Facultad de Ciencias y Educación', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (5,'Facultad del Medio Ambiente y Recursos Naturales', CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


-- Agregar Carreras
INSERT INTO universities_carrera (id_carrera, name_career, id_facultad_fk_id, created, updated)
        VALUES  (1,'Ingeniería Catastral y Geodesia', 2, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Administración Ambiental', 5, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (3,'Administración Deportiva', 4, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (4,'Archivística y Gestión de la Información Digital ', 4, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (5,'Arte Danzario', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (6,'Artes Escénicas', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (7,'Artes Musicales ', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (8,'Artes Plásticas y Visuales', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


-- Agregar Carreras
INSERT INTO teatros_teatro (id_teatro, name_theater, id_facultad_fk_id, created, updated)
        VALUES  (1,'Ingeniería Catastral y Geodesia', 2, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (2,'Administración Ambiental', 5, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (3,'Administración Deportiva', 4, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (4,'Archivística y Gestión de la Información Digital ', 4, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (5,'Arte Danzario', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (6,'Artes Escénicas', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (7,'Artes Musicales ', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
                (8,'Artes Plásticas y Visuales', 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);