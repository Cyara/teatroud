from django.contrib import admin

# Register your models here.
from .models import HorarioTeatro, Teatro, Pais, Dramaturgo, Obra, TipoObra, TipoCalendario, Calendario

class PaisAdmin(admin.ModelAdmin):
    #readonly_fields = ('created', 'updated')
    list_display = ('descripcion','alpha3', )
    ordering = ('alpha3','descripcion')
    search_fields = ('alpha3','descripcion')

class ObraAdmin(admin.ModelAdmin):
    #readonly_fields = ('created', 'updated')
    list_display = ('titulo','fecha_obra', 'id_dramaturgo', 'cod_pais'  )
    list_display_links = ('titulo','fecha_obra', 'id_dramaturgo', 'cod_pais'  )
    ordering = ('titulo','fecha_obra')
    search_fields = ('titulo','id_dramaturgo', 'cod_pais')

admin.site.register(HorarioTeatro)
admin.site.register(Dramaturgo)
admin.site.register(Teatro)
admin.site.register(Obra,ObraAdmin)
admin.site.register(TipoObra)
admin.site.register(Pais, PaisAdmin)
admin.site.register(TipoCalendario)
admin.site.register(Calendario)