from django.apps import AppConfig


class TeatrosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'teatros'
