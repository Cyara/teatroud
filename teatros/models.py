from distutils.command.upload import upload
from email import generator
from tabnanny import verbose
from django.db import models
from django.utils.timezone import datetime

class Pais(models.Model):
    cod_pais = models.CharField(primary_key=True, max_length=3, null=False, unique=True, default='170')
    alpha3 = models.CharField(max_length=3, null=True, unique=True)
    descripcion = models.CharField(max_length=20, null=True, unique=True)
    class Meta:
        verbose_name = "Pais"
        verbose_name_plural = "Paises"
        ordering = ['cod_pais']
    def __str__(self):
        """Return username."""
        return self.descripcion

class TipoObra(models.Model):
    id_tipo_obra = models.CharField(max_length=2,primary_key=True, null=False, unique=True)
    descripcion = models.CharField(max_length=30, default='', null=False, unique=True)
    class Meta:
        verbose_name = "Tipo de obra"
        verbose_name_plural = "Tipos de obras"
        ordering = ['descripcion']
    def __str__(self):
        """Return username."""
        return self.descripcion

class Dramaturgo(models.Model):
    id_dramaturgo = models.CharField(max_length=3, primary_key=True, null=False, unique=True)
    nombre_dramaturgo = models.CharField(max_length=30, default='', null=False, unique=True)
    cod_pais = models.ForeignKey(Pais, default='170', on_delete=models.CASCADE, null=False, blank=False) 
    foto = models.ImageField(upload_to='obras/dramaturgo',null=True, blank=True)   
    class Meta:
        verbose_name = "Dramaturgo"
        verbose_name_plural = "Dramaturgos"
        ordering = ['id_dramaturgo']
    def __str__(self):
        """Return username."""
        return self.nombre_dramaturgo

class HorarioTeatro(models.Model):
    horarioFecha = models.DateField(default=datetime.today, primary_key=True, unique=True, editable=True)
    
    
    class Meta:
        verbose_name = "Horario teatro"
        verbose_name_plural = "Horario teatros"
        ordering = ['horarioFecha']
    def __int__(self):
        """Return username."""
        return self.id_horario

class Teatro(models.Model):
    cod_teatro = models.CharField(max_length=3, default='TTT', primary_key=True, null=False, unique=True)
    nom_teatro = models.CharField(max_length=50, default='TCB', null=False, unique=True)
    dir_teatro = models.CharField(max_length=50, default="Cl. 10 ## 5-32, La Candelaria, Bogotá", null=False, unique=True)
    foto = models.ImageField(upload_to='obras/teatro',null=True, blank=True)
    class Meta:
        verbose_name = "Teatro"
        verbose_name_plural = "Teatros"
        ordering = ['nom_teatro']
    def __str__(self):
        """Return username."""
        return self.nom_teatro

class Obra(models.Model):
    id_obra = models.CharField(max_length=4,primary_key=True, null=False, unique=True)
    id_tipo_obra = models.ManyToManyField(TipoObra, blank=True)
    cod_pais = models.ForeignKey(Pais, default='170', on_delete=models.PROTECT, null=False, blank=False)
    id_dramaturgo = models.ForeignKey(Dramaturgo, default='', on_delete=models.CASCADE, null=False, blank=False)
    fecha_obra = models.DateField(default=datetime.now)
    titulo = models.CharField(max_length=30, null=True, blank=True)
    estado = models.BooleanField(auto_created=False, null=True)
    poster = models.ImageField(upload_to='obras/poster',null=True, blank=True)
    class Meta:
        verbose_name = "Obra"
        verbose_name_plural = "Obras"
        ordering = ['fecha_obra', 'titulo']
    def __str__(self):
        """Return username."""
        return self.titulo

class TipoCalendario(models.Model):
    id_tipo_calendario = models.CharField(max_length=2, primary_key=True, null=False, unique=True)
    desc_tipo_calendario = models.CharField(max_length=20, null=False, unique=True, blank=False)  
    class Meta:
        verbose_name = "Tipo calendario"
        verbose_name_plural = "Tipo calendario"
        ordering = ['desc_tipo_calendario']
    def __str__(self):
        """Return username."""
        return self.desc_tipo_calendario

class Calendario(models.Model):
    consec_calendario = models.DecimalField(max_digits=4, decimal_places=0, primary_key=True, null=False, unique=True)
    id_obra = models.ForeignKey(Obra, default='0001', on_delete=models.CASCADE, null=False, blank=False)
    cod_teatro = models.ForeignKey(Teatro, default='TCB', on_delete=models.CASCADE, null=False, blank=False)
    tipo_calendario = models.ForeignKey(TipoCalendario, default='00', on_delete=models.CASCADE, null=False, blank=False)
    hora_inicio = models.DateField(default=datetime.today, unique=True, editable=True)
    hora_fin = models.DateField(default=datetime.today, unique=True, editable=True)
    class Meta:
        verbose_name = "Calendario"
        verbose_name_plural = "Calendarios"
        ordering = ['consec_calendario']
    def __str__(self):
        """Return username."""
        return self.consec_calendario

