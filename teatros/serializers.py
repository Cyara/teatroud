from rest_framework import serializers
from .models import Pais, TipoCalendario, TipoObra, Dramaturgo, Teatro

class PaisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pais
        fields = '__all__'

class TipoCalendarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoCalendario
        fields = '__all__'

class TipoObraSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoObra
        fields = '__all__'

class DramaturgoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dramaturgo
        fields = '__all__'

class TeatroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teatro
        fields = '__all__'