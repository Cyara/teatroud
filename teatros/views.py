from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from .models import Pais, TipoCalendario, TipoObra, Dramaturgo, Teatro
from .serializers import PaisSerializer, TipoCalendarioSerializer, TipoObraSerializer, DramaturgoSerializer, TeatroSerializer


class PaisViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Pais.objects.all()
    serializer_class = PaisSerializer
    
class TipoCalendarioViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TipoCalendario.objects.all()
    serializer_class = TipoCalendarioSerializer

class TipoObraViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TipoObra.objects.all()
    serializer_class = TipoObraSerializer

class DramaturgoViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Dramaturgo.objects.all()
    serializer_class = DramaturgoSerializer

class TeatroViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Teatro.objects.all()
    serializer_class = TeatroSerializer