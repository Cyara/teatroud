from re import search
from django.contrib import admin
from .models import Carrera, Facultad

# Register your models here.
from .models import Carrera, Facultad

class CarreraAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('id_carrera', 'nom_carrera','id_facultad_fk')
    ordering = ('nom_carrera','id_facultad_fk')
    search_fields = ('id_carrera', 'nom_carrera','id_facultad_fk')
    date_hierarchy = ('created')
    list_filter = ('id_facultad_fk')

admin.site.register(Carrera)
admin.site.register(Facultad)