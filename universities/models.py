from django.db import models

# Create your models here.
class Facultad(models.Model):
    id_facultad = models.IntegerField(primary_key=True, null=False, unique=True)
    nombre_facultad = models.CharField(max_length=50, null=False, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    class Meta:
        verbose_name = "Facultad"
        verbose_name_plural = "Facultades"
        ordering = ['nombre_facultad']
    def __str__(self):
        """Return username."""
        return self.nombre_facultad

# Create your models here.
class Carrera(models.Model):
    id_carrera = models.IntegerField(primary_key=True, null=False, unique=True)
    nom_carrera = models.CharField(max_length=80, null=False, unique=True)
    id_facultad_fk = models.ForeignKey('Facultad', on_delete=models.CASCADE, null=False, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    class Meta:
        verbose_name = "Carrera"
        verbose_name_plural = "Carreras"
        ordering = ['id_facultad_fk','nom_carrera']
    def __str__(self):
        """Return username."""
        return self.nom_carrera
