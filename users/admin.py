from django.contrib import admin

# Register your models her
from .models import Estudiante, Persona,TipoDocumento, Personaje, AsistenciaEstudiante, Unidad, PersonajeEstudiante

class EstudianteAdmin(admin.ModelAdmin):
    #readonly_fields = ('created', 'updated')
    list_display = ('cod_estudiante','apellido','nombre', 'correo')
    list_display_link = ('cod_estudiante')
    ordering = ('apellido','nombre', 'cod_estudiante')
    search_fields = ('cod_estudiante','apellido','nombre', 'correo')

admin.site.register(Estudiante,EstudianteAdmin)
admin.site.register(Persona)
admin.site.register(TipoDocumento)
admin.site.register(Personaje)
admin.site.register(AsistenciaEstudiante)
admin.site.register(Unidad)
admin.site.register(PersonajeEstudiante)
