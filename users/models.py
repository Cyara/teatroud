from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from teatros.models import Obra, Calendario

# Create your models here.
# Tipo Documento
class TipoDocumento(models.Model):
    id_tipo_documento = models.SmallIntegerField(primary_key=True, null=False, unique=True)
    abreviacion = models.CharField(max_length=5, null=False)
    desc_tipo = models.CharField(max_length=50, null=False)
    def __str__(self):
        """Return describe type person."""
        return self.desc_tipo

# Persona
class Persona(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    cod_persona = models.CharField(max_length=11, primary_key=True, null=False, unique=True)
    # To do - Foreing Key
    id_tipo_documento = models.ForeignKey(TipoDocumento, default='no registra', on_delete=models.CASCADE, null=False, blank=False)
    numero_documento = models.CharField(max_length=11, null=False, blank=False, default='00000000000')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return username."""
        return self.user.username

# Estudiante
class Estudiante(models.Model):
    cod_estudiante = models.CharField(max_length=11, primary_key=True, null=False, unique=True)
    nombre = models.CharField(max_length=30, default='no registra', null=False)
    apellido = models.CharField(max_length=30, default='no registra', null=False)
    fecha_inscripcion = models.DateField(default=datetime.today, null=False)
    fecha_nacimiento = models.DateField(default=datetime.today,null=False)
    correo = models.EmailField(max_length=40, default='example@example.com', null=False)
    cod_unidad_estudiante = models.ForeignKey('users.unidad', on_delete=models.CASCADE, default='00001', null=False)
    def __str__(self):
        """Return describe type person."""
        return '{} {}'.format(self.apellido, self.nombre)

class AsistenciaEstudiante(models.Model):
    consec_asistencia = models.DecimalField(max_digits=4, decimal_places=0, primary_key=True)
    conse_calendario = models.ForeignKey(Calendario, on_delete=models.PROTECT, null=False, blank=False, default='1')
    id_obra = models.ForeignKey(Obra, on_delete=models.PROTECT, default='0001', null=False, blank=True)
    cod_estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE, default='20222020001', null=False, blank=True)
    def __str__(self):
        """Return describe type person."""
        return self.cod_estudiante

class Personaje(models.Model):
    id_personaje = models.CharField(max_length=4, primary_key=True, null=False, unique=True)
    id_obra = models.ForeignKey(Obra, on_delete=models.PROTECT, default='0001', null=False, blank=True)
    nom_personaje = models.CharField(max_length=40, null=False, blank=False, default=' ',)
    def __str__(self):
        """Return describe type person."""
        return self.nom_personaje

class PersonajeEstudiante(models.Model):
    consec = models.DecimalField(max_digits=4, decimal_places=0, primary_key=True)
    id_personaje = models.ForeignKey(Personaje, on_delete=models.PROTECT, default='0001', null=False, blank=True)
    id_obra = models.ForeignKey(Obra, on_delete=models.PROTECT, default='0001', null=False, blank=True)
    cod_estudiante = models.ForeignKey(Estudiante, on_delete=models.PROTECT, default='0001', null=False, blank=True)
    fecha_inicio = models.DateField(default=datetime.now)
    fecha_fin = models.DateField(default=datetime.now)
    def __str__(self):
        """Return describe type person."""
        return self.id_personaje

class Unidad(models.Model):
    cod_unidad = models.CharField(max_length=5, primary_key=True, null=False, unique=True)
    tipo_unidad = models.CharField(max_length=2, null=False, blank=True)
    uni_cod_unidad = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    nom_unidad = models.CharField(max_length=40, null=False, blank=True)
    def __str__(self):
        """Return describe type person."""
        return self.nom_unidad

class TipoUnidad(models.Model):
    tipo_unidad = models.CharField(max_length=2, primary_key=True, null=False, unique=True)
    desc_tipo_unidad = models.CharField(max_length=30, null=False, blank=True)
    def __str__(self):
        """Return describe type person."""
        return self.desc_tipo_unidad