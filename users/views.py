from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string, get_template
from django.conf import settings
from django.contrib import messages

# Exception
from django.db.utils import IntegrityError
import time

#Utils
from users.models import Persona
from datetime import datetime

#from .serializers import PersonaSerializer,ComisarioEventoSerializer,TareaSerializer,TipoPersonaSerializer
#from rest_framework import viewsets

# Create your views here.

def home(request):
    return redirect('users:login')

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('home:feed')
            #return redirect('admin:index')
        else:
            return render(request, 'users/login.html', { 'error': 'Usuario o contraseña invalido.'})
    # Si el usuario ya está autenticado
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('home:feed')
    return render(request, 'users/login.html')

@login_required
def logout_view(request):
    logout(request)
    return redirect('users:login')

def signup_view(request):
    if request.method == 'POST':

        username = request.POST['username']
        passwd = request.POST['passwd']
        passwd_confirmation = request.POST['passwd_confirmation']

        if passwd != passwd_confirmation:
            return render(request, 'users/signup_view.html', {'error': 'La confirmación de la contraseña no coincide'})

        #user = User.objects.create_user(username=username, password=passwd)
        try:
            user = User.objects.create_user(username=username, password=passwd)
        except IntegrityError:
            return render(request, 'users/signup_view.html', {'error': 'El nombre de usuario ya está en uso'})

        user.first_name = request.POST['first_name']
        user.last_name = request.POST['last_name']
        user.email = request.POST['email']
        #user.id = request.POST['cod_persona']
        user.save()


        #Enviando correo
        template = render_to_string('email/confirmation.html',{
          'name': (user.first_name+' '+user.last_name),
          'email': user.email,
          }
        )
        #templateGet = get_template('email/confirmation.html')
        #content = templateGet.render({'mail': user.email,})

        subject = 'Su cuenta ha sido creada'
        email = EmailMessage(
          subject,
          template,
          settings.EMAIL_HOST_USER,
          ['correopruebasud@gmail.com'],
        )
        email.content_subtype = "html" 
        email.fail_silently=False
        #email.content_subtype = "html" 
        #email.attach_alternative(template,'text/html')
        email.send()
        messages.success(request, 'Por favor revisa tu correo')
        print('SE ENVÍA CORREO DE PRUEBA')


        persona = Persona(user=user)
        persona.cod_persona = request.POST['cod_persona']
        #persona.id_tipo_persona_fk = request.POST['tipo_persona']
        persona.save()
        return redirect('users:login')

    return render(request, 'users/signup_view.html')

